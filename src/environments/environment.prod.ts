export const environment = {
  production: true,
  backUrl: 'https://backend.can-inn.com/api/',
  frontUrl: 'https://can-inn.com',
};
