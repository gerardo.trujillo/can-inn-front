import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgxSpinnerModule } from "ngx-spinner";
import { HttpClientModule } from "@angular/common/http";
import { SharedModule } from "./components/shared/shared.module";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

@NgModule({
  declarations: [
    AppComponent
  ],
    imports: [
      BrowserModule.withServerTransition({appId: 'serverApp'}),
      AppRoutingModule,
      FontAwesomeModule,
      NgxSpinnerModule,
      HttpClientModule,
      BrowserAnimationsModule,
      SharedModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
