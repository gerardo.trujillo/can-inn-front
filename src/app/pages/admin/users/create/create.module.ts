import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateComponent} from "./create.component";
import { CreateRoutingModule } from "./create-routing.module";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { ComponentsAdminModule } from "../../../../components/admin/components-admin.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FormsModule } from "@angular/forms";



@NgModule({
  declarations: [
    CreateComponent
  ],
  imports: [
    CommonModule,
    CreateRoutingModule,
    FontAwesomeModule,
    ComponentsAdminModule,
    NgbModule,
    FormsModule
  ]
})
export class CreateModule { }
