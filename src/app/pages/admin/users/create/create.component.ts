import { Component, OnInit } from '@angular/core';
import { Title } from "@angular/platform-browser";
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from "@angular/router";
import { NgForm } from "@angular/forms";
import { faHandPointer } from "@fortawesome/free-solid-svg-icons";
import { ToastService } from "../../../../services/components/toast.service";
import { UsersService } from "../../../../services/admin/users.service";

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.sass']
})
export class CreateComponent implements OnInit {

  faHandPointer = faHandPointer;
  type = 'password';

  constructor(private titleService: Title,
              private toastService: ToastService,
              private loading: NgxSpinnerService,
              private service: UsersService,
              private router: Router) {
    this.titleService.setTitle("Nuevo Usuario");
  }

  ngOnInit(): void {
  }

  submit(form: NgForm){
    this.loading.show();
    const params= new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('name', form.value.name);
    params.append('email', form.value.email);
    params.append('password', form.value.password);
    this.service.postUser(params).subscribe(response => {
      this.loading.hide();
      this.router.navigateByUrl('/admin/users');
    }, error => {
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }


  eyePass(){
    if(this.type == 'password'){
      this.type = 'text';
    } else {
      this.type = 'password';
    }
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

}
