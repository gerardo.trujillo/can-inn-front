import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { NgxSpinnerService } from 'ngx-spinner';
import { faTable, faPlus, faPowerOff, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { User } from "../../../../interfaces/user";
import {ToastService} from "../../../../services/components/toast.service";
import {UsersService} from "../../../../services/admin/users.service";
import {StorageService} from "../../../../services/storage/storage.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  faTable = faTable;
  faPlus = faPlus;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  users: User[]=[];
  public page: number | undefined;

  constructor(private titleService: Title,
              private toastService: ToastService,
              private loading: NgxSpinnerService,
              private service: UsersService,
              public storage: StorageService) {
    this.titleService.setTitle("Lista de Usuarios");
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.loading.show();
    this.service.getUsers().subscribe(response => {
      this.users = response.users;
      this.loading.hide();
    }, error => {
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  active(user: string){
    this.loading.show();
    this.service.activeUser(user).subscribe(response => {
      this.showSuccess(`El usuario ${response.user.name} se activo con exíto`);
      this.getData();
      this.loading.hide();
    }, error => {
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  desActive(user: string){
    this.loading.show();
    this.service.desActiveUser(user).subscribe(response => {
      this.showSuccess(`El usuario ${response.user.name} se desactivo con exíto`);
      this.getData();
      this.loading.hide();
    }, error => {
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  delete(user: string){
    this.loading.show();
    this.service.deleteUser(user).subscribe(response => {
      this.showSuccess(`El usuario ${response.user.name} se elimino con exíto`);
      this.getData();
      this.loading.hide();
    }, error => {
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }


}
