import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from "./list.component";
import { ListRoutingModule } from "./list-routing.module";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { NgxPaginationModule } from "ngx-pagination";
import { ComponentsAdminModule } from "../../../../components/admin/components-admin.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";



@NgModule({
  declarations: [
    ListComponent
  ],
  imports: [
    CommonModule,
    ListRoutingModule,
    FontAwesomeModule,
    NgxPaginationModule,
    ComponentsAdminModule,
    NgbModule,
  ]
})
export class ListModule { }
