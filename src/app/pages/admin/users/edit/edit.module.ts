import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditComponent } from "./edit.component";
import { EditRoutingModule } from "./edit-routing.module";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { ComponentsAdminModule } from "../../../../components/admin/components-admin.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FormsModule } from "@angular/forms";



@NgModule({
  declarations: [
    EditComponent
  ],
  imports: [
    CommonModule,
    EditRoutingModule,
    FontAwesomeModule,
    ComponentsAdminModule,
    NgbModule,
    FormsModule
  ]
})
export class EditModule { }
