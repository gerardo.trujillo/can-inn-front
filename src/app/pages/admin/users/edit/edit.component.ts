import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { User } from "../../../../interfaces/user";
import { ActivatedRoute, Router } from "@angular/router";
import { Title } from "@angular/platform-browser";
import { NgxSpinnerService } from "ngx-spinner";
import { NgForm } from "@angular/forms";
import { faHandPointer } from '@fortawesome/free-solid-svg-icons';
import {UsersService} from "../../../../services/admin/users.service";
import {ToastService} from "../../../../services/components/toast.service";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.sass']
})
export class EditComponent implements OnInit {

  // @ts-ignore
  @ViewChild('checkbox') checkbox: ElementRef;
  user:User=<User>{};
  faHandPointer = faHandPointer;
  passwordV= false;
  type = 'password';

  constructor(private activatedRoute: ActivatedRoute,
              private titleService: Title,
              private service: UsersService,
              private toastService: ToastService,
              private loading: NgxSpinnerService,
              private router: Router) {
    this.activatedRoute.params.subscribe( params => {
      this.getData(params['id']);
    });
  }

  ngOnInit(): void {
  }

  getData(id: string){
    this.loading.show();
    this.service.showUser(id).subscribe(response => {
      console.log(response);
      this.user = response.user;
      this.loading.hide();
    }, error => {
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  changePassword(){
    if (this.checkbox.nativeElement.checked){
      this.passwordV= true;
    } else {
      this.passwordV= false;
    }
  }

  submit(form: NgForm){
    this.loading.show();
    const params= new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('name', form.value.name);
    params.append('email', form.value.email);
    params.append('password', form.value.password);
    this.service.putUser(this.user.uid, params).subscribe(response => {
      this.loading.hide();
      this.router.navigateByUrl('/admin/users');
    }, error => {
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  eyePass(){
    if(this.type == 'password'){
      this.type = 'text';
    } else {
      this.type = 'password';
    }
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

}
