import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditComponent } from "./edit.component";
import { EditRoutingModule } from "./edit-routing.module";
import { ComponentsAdminModule } from "../../../../components/admin/components-admin.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { NgxPaginationModule } from "ngx-pagination";
import { NgxDropzoneModule } from "ngx-dropzone";
import { FormsModule } from "@angular/forms";
import { CKEditorModule } from "@ckeditor/ckeditor5-angular";
import {PipesModule} from "../../../../pipes/pipes.module";



@NgModule({
  declarations: [
    EditComponent
  ],
    imports: [
        CommonModule,
        EditRoutingModule,
        ComponentsAdminModule,
        NgbModule,
        FontAwesomeModule,
        NgxPaginationModule,
        NgxDropzoneModule,
        FormsModule,
        CKEditorModule,
        PipesModule
    ]
})
export class EditModule { }
