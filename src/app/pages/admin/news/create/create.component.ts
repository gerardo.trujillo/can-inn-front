import { Component, OnInit } from '@angular/core';
import { faList, faSave, faEye, faEyeSlash, faImages, faHandPointer } from '@fortawesome/free-solid-svg-icons';
import { environment } from "../../../../../environments/environment";
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from "@angular/router";
import { Title } from "@angular/platform-browser";
import { NgForm } from "@angular/forms";
import { NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import { ToastService } from "../../../../services/components/toast.service";
import { NewsService } from "../../../../services/admin/news.service";

// @ts-ignore
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';



@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.sass']
})
export class CreateComponent implements OnInit {

  faList = faList;
  faHandPointer = faHandPointer;
  faImages = faImages;
  faSave = faSave;
  faEyeSlash = faEyeSlash;
  faEye = faEye;
  url_images = environment.backUrl;
  public Editor = ClassicEditor;
  image:any;
  imageInit:any;
  thumbnail:any;
  editImage = true;
  model: NgbDateStruct = <NgbDateStruct>{};
  date: { year: number; month: number; } | undefined;
  files: File[] = [];

  constructor(private toastService: ToastService,
              private loading: NgxSpinnerService,
              private service: NewsService,
              private router: Router,
              private titleService: Title,
              private calendar: NgbCalendar) {
    this.titleService.setTitle("Crear Noticia");
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){

  }

  selectToday() {
    this.model = this.calendar.getToday();
  }

  submit(form: NgForm) {
    this.loading.show();
    let month = '';
    let day = '';
    let date = '';
    if (this.isString(form.value.date)){
      date = form.value.date;
    } else {
      if (form.value.date.month < 10 ){
        month = '0'+form.value.date.month;
      } else {
        month = form.value.date.month;
      }
      if (form.value.date.day < 10){
        day = '0' + form.value.date.day;
      } else {
        day = form.value.date.day;
      }
      date = form.value.date.year + '-' + month + '-' + day;
    }
    const params= new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('title', form.value.title);
    params.append('content', form.value.content);
    params.append('intro', form.value.intro);
    params.append('date', date);
    this.service.postNews(params).subscribe(response => {
      this.loading.hide();
      this.router.navigateByUrl('/admin/news');
    }, error => {
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  getImage(e: any){
    let file = e.target.files[0];
    this.image = file;
    this.uploadImage(file);
  }

  uploadImage(file: any){
    let reader = new FileReader();
    reader.onload = (e) => {
      //@ts-ignore
      this.thumbnail = e.target.result;
    }

    reader.readAsDataURL(file);
  }

  isString(text:any){
    if(typeof text === 'string' || text instanceof String){
      return true;
    }else{
      return false;
    }
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

}
