import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from "./list.component";
import { ListRoutingModule } from "./list-routing.module";
import { NgxPaginationModule } from "ngx-pagination";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { ComponentsAdminModule } from "../../../../components/admin/components-admin.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import {PipesModule} from "../../../../pipes/pipes.module";



@NgModule({
  declarations: [
    ListComponent
  ],
    imports: [
        CommonModule,
        ListRoutingModule,
        ComponentsAdminModule,
        NgbModule,
        FontAwesomeModule,
        NgxPaginationModule,
        PipesModule,
    ]
})
export class ListModule { }
