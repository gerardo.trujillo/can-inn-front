import { Component, OnInit } from '@angular/core';
import { faTable, faPlus, faPowerOff, faTrashAlt, faCertificate, faStar } from '@fortawesome/free-solid-svg-icons';
import { News } from "../../../../interfaces/news";
import { environment } from "../../../../../environments/environment";
import { Title } from "@angular/platform-browser";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastService} from "../../../../services/components/toast.service";
import { StorageService } from "../../../../services/storage/storage.service";
import { NewsService } from "../../../../services/admin/news.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  faTable = faTable;
  faCertificate = faCertificate;
  faStar = faStar;
  faPlus = faPlus;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  news: News[]=[];
  public page: number | undefined;
  url_images = environment.backUrl;

  constructor(private titleService: Title,
              private toastService: ToastService,
              private loading: NgxSpinnerService,
              private service: NewsService,
              public storage: StorageService) {
    this.titleService.setTitle("Lista de Noticias");
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.loading.show();
    this.service.getNews().subscribe(response => {
      this.news = response.news;
      this.loading.hide();
    }, error => {
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  active(news: string){
    this.loading.show();
    this.service.activeNews(news).subscribe(response => {
      this.showSuccess(`La noticia ${response.news.title} se activo con exíto`);
      this.getData();
      this.loading.hide();
    }, error => {
      if(error.status == 400){
        if (error.error.errors.length > 0){
          for (let i=0; error.error.errors.length>i;i++){
            this.showDanger(error.error.errors[0].msg);
          }
        } else {
          this.showDanger(error.error.msg);
        }
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  desActive(news: string){
    this.loading.show();
    this.service.desActiveNews(news).subscribe(response => {
      this.showSuccess(`La noticia ${response.news.title} se desactivo con exíto`);
      this.getData();
      this.loading.hide();
    }, error => {
      if(error.status == 400){
        if (error.error.errors.length > 0){
          for (let i=0; error.error.errors.length>i;i++){
            this.showDanger(error.error.errors[0].msg);
          }
        } else {
          this.showDanger(error.error.msg);
        }
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  delete(news: string){
    this.loading.show();
    this.service.deleteNews(news).subscribe(response => {
      this.showSuccess(`La noticia ${response.news.title} se elimino con exíto`);
      this.getData();
      this.loading.hide();
    }, error => {
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

  replaceImage(image: any){
    image.onerror = '';
    image.src = 'assets/images/missing.png'
  }

}
