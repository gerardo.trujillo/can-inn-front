import {Component, HostListener, OnInit} from '@angular/core';
import {Meta, Title} from "@angular/platform-browser";
import {ComponentsService} from "../../services/admin/components.service";
import {NgxSpinnerService} from "ngx-spinner";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.sass']
})
export class AdminComponent implements OnInit {

  @HostListener('window:resize', ['$event'])
  onResize(event: Event) {
    if (window.innerWidth < 769) {
      this.serviceMenu.setClose(true);
      this.serviceMenu.setBars(true);
    } else {
      this.serviceMenu.setClose(false);
      this.serviceMenu.setBars(false);
    }
  }
  constructor(private title: Title,
              private meta: Meta,
              public serviceMenu: ComponentsService,
              private loading: NgxSpinnerService) {
    this.title.setTitle('Administrador');
  }

  ngOnInit(): void {
  }

}
