import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CarrouselRoutingModule } from './carrousel-routing.module';
import { CarrouselComponent } from './carrousel.component';


@NgModule({
  declarations: [
    CarrouselComponent
  ],
  imports: [
    CommonModule,
    CarrouselRoutingModule
  ]
})
export class CarrouselModule { }
