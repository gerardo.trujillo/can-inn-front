import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditRoutingModule } from './edit-routing.module';
import { EditComponent } from './edit.component';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {ComponentsAdminModule} from "../../../../components/admin/components-admin.module";
import {FormsModule} from "@angular/forms";
import {CKEditorModule} from "@ckeditor/ckeditor5-angular";
import {PipesModule} from "../../../../pipes/pipes.module";


@NgModule({
  declarations: [
    EditComponent
  ],
    imports: [
        CommonModule,
        EditRoutingModule,
        FontAwesomeModule,
        ComponentsAdminModule,
        FormsModule,
        CKEditorModule,
        PipesModule
    ]
})
export class EditModule { }
