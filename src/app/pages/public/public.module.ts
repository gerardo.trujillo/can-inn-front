import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicRoutingModule } from './public-routing.module';
import { PublicComponent } from './public.component';
import { ComponentsPublicModule } from "../../components/public/components-public.module";
import { IvyGalleryModule } from "angular-gallery";


@NgModule({
  declarations: [
    PublicComponent
  ],
  imports: [
    CommonModule,
    PublicRoutingModule,
    ComponentsPublicModule,
    IvyGalleryModule,
  ]
})
export class PublicModule { }
