import {Component, OnInit, ViewChild} from '@angular/core';
import { NewsService } from "../../../services/public/news.service";
import { Title } from "@angular/platform-browser";
import { ToastService } from "../../../services/components/toast.service";
import { NgxSpinnerService } from "ngx-spinner";
import { News } from "../../../interfaces/news";
import {NgbCarousel, NgbSlideEvent, NgbSlideEventSource} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.sass']
})
export class MainComponent implements OnInit {

  news:News[]=[];
  promotions:any[]=[];

  paused = false;
  unpauseOnArrow = false;
  pauseOnIndicator = false;
  pauseOnHover = true;
  pauseOnFocus = true;

  // @ts-ignore
  @ViewChild('carousel', {static : true}) carousel: NgbCarousel;

  constructor(private newsService: NewsService,
              private titleService: Title,
              private toastService: ToastService,
              private loading: NgxSpinnerService) {
    this.titleService.setTitle('Can Inn');
  }

  ngOnInit(): void {
    this.getData();
  }

  togglePaused() {
    if (this.paused) {
      this.carousel.cycle();
    } else {
      this.carousel.pause();
    }
    this.paused = !this.paused;
  }

  onSlide(slideEvent: NgbSlideEvent) {
    if (this.unpauseOnArrow && slideEvent.paused &&
      (slideEvent.source === NgbSlideEventSource.ARROW_LEFT || slideEvent.source === NgbSlideEventSource.ARROW_RIGHT)) {
      this.togglePaused();
    }
    if (this.pauseOnIndicator && !slideEvent.paused && slideEvent.source === NgbSlideEventSource.INDICATOR) {
      this.togglePaused();
    }
  }

  getData(){
    this.loading.show();
    this.newsService.getNews().subscribe(response => {
      this.news = response.news;
      this.promotions = response.promotions;
      this.loading.hide();
    }, error => {
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

  replaceImage(image: any){
    image.onerror = '';
    image.src = 'assets/images/missing.png'
  }

}
