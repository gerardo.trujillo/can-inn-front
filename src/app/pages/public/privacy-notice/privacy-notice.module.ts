import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrivacyNoticeRoutingModule } from './privacy-notice-routing.module';
import { PrivacyNoticeComponent } from './privacy-notice.component';


@NgModule({
  declarations: [
    PrivacyNoticeComponent
  ],
  imports: [
    CommonModule,
    PrivacyNoticeRoutingModule
  ]
})
export class PrivacyNoticeModule { }
