import { Component, OnInit } from '@angular/core';
import {DomSanitizer, Meta, Title} from "@angular/platform-browser";
import {ActivatedRoute} from "@angular/router";
import {ToastService} from "../../../services/components/toast.service";
import {NgxSpinnerService} from "ngx-spinner";
import {Gallery} from "angular-gallery";
import {environment} from "../../../../environments/environment";
import {Image} from "../../../interfaces/image";
import {PromotionsService} from "../../../services/public/promotions.service";

@Component({
  selector: 'app-promotion',
  templateUrl: './promotion.component.html',
  styleUrls: ['./promotion.component.sass']
})
export class PromotionComponent implements OnInit {

  promotion:Image=<Image>{};
  linkface:any;
  url_image:string='';
  url = environment.frontUrl;

  constructor(private promotionsService: PromotionsService,
              private titleService: Title,
              private sanitizer: DomSanitizer,
              private activatedRouter: ActivatedRoute,
              private toastService: ToastService,
              private loading: NgxSpinnerService,
              private meta: Meta,
              private gallery: Gallery) {
    this.activatedRouter.params.subscribe(params => {
      this.getData(params['id']);
    });
  }

  ngOnInit(): void {
  }

  getData(id: string){
    this.loading.show();
    this.promotionsService.showPromotion(id).subscribe(response => {
      this.promotion = response.promotion;
      this.url_image =  environment.backUrl + 'uploads/promotions/' + this.promotion._id;
      let link = `https://www.facebook.com/plugins/like.php?href=${this.url}%2Fpromocion%2F${this.promotion._id}
      &width=450&layout=standard&action=like&size=small&share=true&height=35&appId=968660020378271`;
      this.linkface = this.sanitizer.bypassSecurityTrustResourceUrl(link);
      let description = this.promotion.description.replace(/<[^>]*>?/g, '');
      this.meta.addTags([
        { property: 'og:url', content: this.url + '/promocion/' + this.promotion._id},
        { property: 'og:type', content: 'website' },
        { property: 'og:description', content: description },
        { property: 'og:title', content: this.promotion.title },
        { property: 'og:image', content: this.url_image }
      ]);
      this.titleService.setTitle(this.promotion.title);
      this.loading.hide();
    }, error => {
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

  replaceImage(image: any){
    image.onerror = '';
    image.src = 'assets/images/missing.png'
  }

}
