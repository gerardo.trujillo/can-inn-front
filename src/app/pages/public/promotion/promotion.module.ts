import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PromotionRoutingModule } from './promotion-routing.module';
import { PromotionComponent } from './promotion.component';
import {PipesModule} from "../../../pipes/pipes.module";


@NgModule({
  declarations: [
    PromotionComponent
  ],
    imports: [
        CommonModule,
        PromotionRoutingModule,
        PipesModule
    ]
})
export class PromotionModule { }
