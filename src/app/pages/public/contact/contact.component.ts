import { Component, OnInit } from '@angular/core';
import { Title } from "@angular/platform-browser";
import { NgForm } from "@angular/forms";

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.sass']
})
export class ContactComponent implements OnInit {

  recaptcha:boolean=false;

  constructor(private title: Title) {
    this.title.setTitle('Contacto');
  }

  ngOnInit(): void {
  }

  sendEmail(form: NgForm) {
    if (!this.recaptcha){
      alert('Debes comprobar que no eres robot')
    } else {
      alert('Mensaje enviado');
    }
  }

  resolved(captchaResponse: string) {
    console.log(`Resolved captcha with response: ${captchaResponse}`);
    this.recaptcha = true;
  }

}
