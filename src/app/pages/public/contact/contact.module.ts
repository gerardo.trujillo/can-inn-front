import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContactRoutingModule } from './contact-routing.module';
import { ContactComponent } from './contact.component';
import {FormsModule} from "@angular/forms";
import {RecaptchaModule} from "ng-recaptcha";


@NgModule({
  declarations: [
    ContactComponent
  ],
  imports: [
    CommonModule,
    ContactRoutingModule,
    FormsModule,
    RecaptchaModule
  ]
})
export class ContactModule { }
