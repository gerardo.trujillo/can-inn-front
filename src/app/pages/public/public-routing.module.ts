import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PublicComponent } from './public.component';

const routes: Routes = [
  {
    path: '',
    component: PublicComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./main/main.module').then(m => m.MainModule)
      },
      {
        path: 'aviso-privacidad',
        loadChildren: () => import('./privacy-notice/privacy-notice.module').then(m => m.PrivacyNoticeModule)
      },
      {
        path: 'contacto',
        loadChildren: () => import('./contact/contact.module').then(m => m.ContactModule)
      },
      {
        path: 'noticia/:id',
        loadChildren: () => import('./news/news.module').then(m => m.NewsModule)
      },
      {
        path: 'promocion/:id',
        loadChildren: () => import('./promotion/promotion.module').then(m => m.PromotionModule)
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutingModule { }
