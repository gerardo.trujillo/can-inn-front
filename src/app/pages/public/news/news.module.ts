import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewsRoutingModule } from './news-routing.module';
import { NewsComponent } from './news.component';
import {PipesModule} from "../../../pipes/pipes.module";


@NgModule({
  declarations: [
    NewsComponent
  ],
    imports: [
        CommonModule,
        NewsRoutingModule,
        PipesModule
    ]
})
export class NewsModule { }
