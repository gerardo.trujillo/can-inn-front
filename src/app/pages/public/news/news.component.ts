import { Component, OnInit } from '@angular/core';
import { News} from "../../../interfaces/news";
import { NewsService } from "../../../services/public/news.service";
import { DomSanitizer, Meta, Title } from "@angular/platform-browser";
import { ToastService } from "../../../services/components/toast.service";
import { NgxSpinnerService } from "ngx-spinner";
import { ActivatedRoute } from "@angular/router";
import { Gallery } from "angular-gallery";
import { environment } from "../../../../environments/environment";

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.sass']
})
export class NewsComponent implements OnInit {

  news:News=<News>{};
  images:any[]=[];
  linkface:any;
  url_images:string='';
  url = environment.frontUrl;

  constructor(private newsService: NewsService,
              private titleService: Title,
              private sanitizer: DomSanitizer,
              private activatedRouter: ActivatedRoute,
              private toastService: ToastService,
              private loading: NgxSpinnerService,
              private meta: Meta,
              private gallery: Gallery) {
    this.activatedRouter.params.subscribe(params => {
      this.getData(params['id']);
    });
  }

  ngOnInit(): void {
  }

  getData(id: string){
    this.loading.show();
    this.newsService.showNews(id).subscribe(response => {
      this.news = response.news;
      this.images = response.news.images;
      this.url_images =  environment.backUrl + 'uploads/news/' + this.news._id;
      let link = `https://www.facebook.com/plugins/like.php?href=${this.url}%2Fnoticia%2F${this.news.slug}
      &width=450&layout=standard&action=like&size=small&share=true&height=35&appId=968660020378271`;
      this.linkface = this.sanitizer.bypassSecurityTrustResourceUrl(link);
      let description = this.news.content.replace(/<[^>]*>?/g, '');
      this.meta.addTags([
        { property: 'og:url', content: this.url + '/noticia/' + this.news.slug},
        { property: 'og:type', content: 'website' },
        { property: 'og:description', content: description },
        { property: 'og:title', content: this.news.title },
        { property: 'og:image', content: this.url_images }
      ]);
      this.titleService.setTitle(this.news.title);
      this.loading.hide();
    }, error => {
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

  replaceImage(image: any){
    image.onerror = '';
    image.src = 'assets/images/missing.png'
  }

  showGallery(index: number) {
    let images:any[]=[];
    images.push({path: this.url_images})
    for(let i=0; i<this.images.length; i++){
      images.push({path: this.url_images + '/' + this.images[i]})
    }
    let prop = {
      images: images,
      index
    };
    this.gallery.load(prop);
  }

}
