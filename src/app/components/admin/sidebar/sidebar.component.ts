import { Component, OnInit } from '@angular/core';
import { faTachometerAlt, faWarehouse, faUser, faImages, faNewspaper, faBlog, faUsers } from '@fortawesome/free-solid-svg-icons';
import { ComponentsService } from '../../../services/admin/components.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.sass']
})
export class SidebarComponent implements OnInit {

  faTachometerAlt = faTachometerAlt;
  faUsers = faUsers;
  faNewspaper = faNewspaper;
  faBlog = faBlog;
  faUser = faUser;
  faImages = faImages;

  constructor(public serviceMenu: ComponentsService) { }

  ngOnInit(): void {
  }

  closed(){
    if (this.serviceMenu.getClose()){
      this.serviceMenu.setClose(false);
    } else {
      this.serviceMenu.setClose(true);
    }
  }

}
