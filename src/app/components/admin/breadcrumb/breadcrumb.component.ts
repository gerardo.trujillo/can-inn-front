import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.sass']
})
export class BreadcrumbComponent implements OnInit {

  @Input() titlePage: string = '';
  @Input() backPage: string = '';
  @Input() url: string = '';
  @Input() single: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

}
