export interface User {
  uid: string;
  name: string;
  email: string;
  password: string;
  pass: string;
  avatar: string;
  active: boolean;
}
