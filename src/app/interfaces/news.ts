export interface News {
  _id: string
  title: string;
  slug: string;
  intro: string;
  content: string;
  images: [];
  date: string;
  image: string;
  active: boolean;
}
