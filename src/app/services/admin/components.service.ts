import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ComponentsService {

  closeBar = false;
  faIcon = false;

  constructor() { }

  setClose(option: boolean){
    this.closeBar = option;
  }

  getClose(){
    return this.closeBar;
  }

  setBars(option: boolean){
    this.faIcon = option;
  }

  getBars(){
    return this.faIcon;
  }
}
