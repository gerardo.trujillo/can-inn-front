import { Injectable } from '@angular/core';
import { environment} from "../../../environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { StorageService } from "../storage/storage.service";

@Injectable({
  providedIn: 'root'
})
export class PromotionsService {

  url = environment.backUrl + 'promotions';
  token:string='';
  headers:any;

  constructor(private http: HttpClient,
              private storage: StorageService) {
    this.token = this.storage.getToken();
    this.headers = new HttpHeaders({
      'x-token': this.token,
    });
  }

  getPromotions(){
    return this.http.get<any>(this.url, {headers: this.headers});
  }
  postPromotion(params: any){
    return this.http.post<any>(this.url, params, {headers: this.headers});
  }
  putPromotion(uid:string, params: any){
    return this.http.put<any>(this.url+ '/' + uid, params, {headers: this.headers});
  }
  showPromotion(uid: string){
    return this.http.get<any>(this.url + '/' + uid, {headers: this.headers});
  }
  activePromotion(uid: string){
    return this.http.get<any>(this.url + '/active/' + uid, {headers: this.headers});
  }
  desActivePromotion(uid: string){
    return this.http.get<any>(this.url + '/des-active/' + uid, {headers: this.headers});
  }
  deletePromotion(uid: string){
    return this.http.delete<any>(this.url+ '/' + uid, {headers: this.headers});
  }
}
