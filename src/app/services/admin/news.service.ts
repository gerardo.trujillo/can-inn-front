import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {StorageService} from "../storage/storage.service";

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  url = environment.backUrl + 'news';
  token:string='';
  headers:any;

  constructor(private http: HttpClient,
              private storage: StorageService) {
    this.token = this.storage.getToken();
    this.headers = new HttpHeaders({
      'x-token': this.token,
    });
  }

  getNews(){
    return this.http.get<any>(this.url, {headers: this.headers});
  }
  postNews(params: any){
    return this.http.post<any>(this.url, params, {headers: this.headers});
  }
  putNews(uid:string, params: any){
    return this.http.put<any>(this.url+ '/' + uid, params, {headers: this.headers});
  }
  showNews(uid: string){
    return this.http.get<any>(this.url + '/' + uid, {headers: this.headers});
  }
  activeNews(uid: string){
    return this.http.get<any>(this.url + '/active/' + uid, {headers: this.headers});
  }
  desActiveNews(uid: string){
    return this.http.get<any>(this.url + '/des-active/' + uid, {headers: this.headers});
  }
  deleteNews(uid: string){
    return this.http.delete<any>(this.url+ '/' + uid, {headers: this.headers});
  }
}
