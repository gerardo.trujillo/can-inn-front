import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class PromotionsService {

  url = environment.backUrl + 'promotions/public';
  token:string='';
  headers:any;

  constructor(private http: HttpClient) {}

  getPromotions(){
    return this.http.get<any>(this.url);
  }

  showPromotion(id: string){
    return this.http.get<any>(this.url + '/' + id);
  }

}
