import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  url = environment.backUrl + 'news/public';
  token:string='';
  headers:any;

  constructor(private http: HttpClient) {}

  getNews(){
    return this.http.get<any>(this.url);
  }

  showNews(id: string){
    return this.http.get<any>(this.url + '/' + id);
  }

}
